﻿var DA = {
    histCount: 0,
    varCount: 0,
    rbCount: 0,
    varList: {},

    rGetVar: /^([\w$]+)\s*=/,
    rVar: /^var\s+/,
    rFunc: /^function\s+([\w$]+)\s*\(/,
    r1: /<i>\d+\s<\/i>/,

    breakCmd: function (cmdstr, sEnd) {
        var stack = [0, 0, 0, 0, 0],   // 分别是 { ( [ " ' 的个数
            index = [0], i, len;
        // 断句
        for (i = 0, len = cmdstr.length; i < len; ++i) {
            switch (cmdstr[i]) {
                case "{":
                    ++stack[0];
                    break;
                case "(":
                    ++stack[1];
                    break;
                case "[":
                    ++stack[2];
                    break;
                case "\"":
                    ++stack[3];
                    break;
                case "'":
                    ++stack[4];
                    break;
                case "}":
                    --stack[0];
                    break;
                case ")":
                    --stack[1];
                    break;
                case "]":
                    --stack[2];
                    break;
                case ";":
                case "\n":
                    if (cmdstr[i] == "\n" && (cmdstr[i - 1] == ";" && cmdstr[i - 1] == "\n")) {
                        break;
                    }
                    if (stack[0] == 0 && stack[1] == 0 && stack[2] == 0 && (stack[3] % 2 == 0) && (stack[4] % 2 == 0)) {
                        index.push(i + 1);  // i+1 把分号划到前一句
                    }
                    break;
            }
        }
        if (!sEnd) {
            index.push(len);    // 不是 len-1, substring(0,len)才能把字符串取全
        }
        return index;
    },
    updateVarList: function (vars, sEnd) {
        var vLen = vars.length,
            vId, vName, vHtml;
        for (i = 0; i < vLen; ++i) {
            vName = vars[i];
            vHtml = this.printVar(vName);
            if (vId = this.varList[vName]) {
                UI.changeVar(vId, vHtml);
            } else {
                vId = this.varList[vName] = ++this.varCount;
                UI.addVar(vId, vName, vHtml);
            }
        }
        // 在 cmdlist 中输出最后一个命令的结果
        if (!sEnd) {
            UI.addResult(vName, vHtml);
        }
    },
    printMatrix: function (mat) {
        var m = mat.length;
        if (!m) return "(空数组)";
        var str = "<table class='matrix'>",
            n, i, j;
        for (i = 0; i < m; ++i) {
            str += "<tr>";
            if (mat[i].constructor == Array) {
                n = mat[0].length;
                for (j = 0; j < n; ++j) {
                    str += "<td>" + mat[i][j] + "</td>";
                }
            } else {
                str += "<td>" + mat[i] + "</td>";
            }
            str += "</tr>";
        }
        str += "</table>";
        return str;
    },
    printVar: function (name) {
        var rHtml;
        name = eval(name);
        if (name) {
            rHtml = name.constructor == Array ? this.printMatrix(name) : name;
        } else {
            switch (typeof name) {
                case "boolean":
                    rHtml = "false";
                    break;
                case "object":
                    rHtml = "null";
                    break;
                case "undefined":
                    rHtml = "undefined";
                    break;
                case "number":
                    if (name === 0) {
                        rHtml = "0";
                    } else {
                        rHtml = "NaN";
                    }
                    break;
                case "string":
                    rHtml = "''";
                    break;
            }
        }
        return rHtml;
    },
    run: function (cmdstr, isCmdMode) {
        var sEnd = /;$/.test(cmdstr),   // 是否以分号结尾
            index = this.breakCmd(cmdstr, sEnd),
            indexLen = index.length,
            i, vars = [];

        for (i = 0; i < indexLen - 1; ++i) {
            var cmdline = $.trim(cmdstr.substring(index[i], index[i + 1])),
                result;
            // 删除开始的var 和function
            cmdline = cmdline.replace(this.rVar, '');
            cmdline = cmdline.replace(this.rFunc, '$1=function(');
            var match = cmdline.match(this.rGetVar);

            try {
                result = eval(cmdline);
            } catch (e) {
                alert("错误在第" + (i + 1) + "个语句块\n" + e);
                return;
            }
            if (match) {
                vars.push(match[1]);
            } else {
                vars.push("ans");
                ans = result;
            }
        }
        // 计数
        this.rbCount = ++this.histCount + 1;    // 最后加 1 因为 rollBack 时先减了 1
        // 添加命令列表
        UI.addCmdList(cmdstr);
        // 添加历史记录
        UI.addHist(this.histCount, cmdstr);
        // 更新变量列表
        this.updateVarList(vars, sEnd);
        // scroll
        UI.scrollCmdList();
        // 重置输入框
        if (isCmdMode) {
            UI.clearCmd();
        }
    }
};
var UI = {
    curMode: $("#cmd"),
    curMenu: [],
    resize: function () {
        $(".content").height($(window).height() - 50);
    },
    changeVar: function (vId, vHtml) {
        $("#v" + vId).html("" + vHtml);
    },
    addVar: function (vId, vName, vHtml) {
        $("#variables").append("<li><i>" + vId + " </i>" + vName + " =\
                                <div id='v" + vId + "'>" + vHtml + "</div></li>");
    },
    addResult: function (vName, vHtml) {
        $("#cmdlist li:last-child").append("<div class='var'>" + vName + " =</div>\
                                            <div class='result'>" + vHtml + "</div>");
    },
    addCmdList: function (cmdstr) {
        $("#cmdlist").append("<li><div class='cmdstr'>" + cmdstr + "</div></li>");
    },
    addHist: function (histCount, cmdstr) {
        $("#history").append("<li><i>" + histCount + " </i><span id='h" + histCount + "'>" + cmdstr + "</span></li>");
    },
    scrollCmdList: function () {
        $("#middle-content").scrollTop(1000000);
    },
    clearCmd: function () {
        $("#cmd").val("");
    },
    rollBack: function (forward, hId) {
        if (!hId) {
            if (forward) {
                --DA.rbCount;
            } else {
                ++DA.rbCount;
            }
            DA.rbCount = Math.max(1, Math.min(DA.histCount, DA.rbCount));
            hId = DA.rbCount;
        }
        $("#cmd").val($("#h" + hId).html());
    },
    // show drop menu
    dropMenu: function (elem, menu) {
        var offset = elem.offset();
        menu.data("prevElem", elem).css({ top: offset.top + elem.height(), left: offset.left }).show();
        this.curMenu.push(menu);
        elem.addClass("current");
    },
    // show sub menu
    subMenu: function (obj, menu) {
        var offset = obj.offset();
        menu.data("prevElem", obj).css({ top: offset.top, left: offset.left + obj.width() + 6 }).show();
        this.curMenu.push(menu);
    },
    clearMenu: function () {
        for (var i = 0, len = this.curMenu.length; i < len; ++i) {
            this.curMenu[i].hide().data("prevElem").removeClass("current");
        }
        this.curMenu = [];
    },
    // === 组件 === Tabs Resize Drag ===
    Tabs: function (tabs, contents) {
        tabs.each(function (i) {
            $(this).click(function () {
                tabs.removeClass("tab-cur");
                $(this).addClass("tab-cur");
                contents.hide().eq(i).show();
            });
        });
    },
    Resize: function (target, handler, callback) {
        handler.mousedown(function (oEvent) {
            var oX = oEvent.pageX, oY = oEvent.pageY;
            function tmp(nEvent) {
                var dX = nEvent.pageX - oX,
                dY = nEvent.pageY - oY;
                oX = nEvent.pageX;
                oY = nEvent.pageY;
                target.each(function () {
                    if (this.width) {
                        this.setAttribute("width", parseInt(this.getAttribute("width")) + dX);
                        this.setAttribute("height", parseInt(this.getAttribute("height")) + dY);
                        //this.width = parseInt(this.width) + dX;
                        //this.height = parseInt(this.height) + dY;
                    } else {
                        var o = $(this);
                        o.width(o.width() + dX).height(o.height() + dY);
                    }
                });
                handler.css({ top: parseInt(handler.css("top")) + dY, left: parseInt(handler.css("left")) + dX });
            }
            $(document).mousemove(tmp).mouseup(function () {
                if (callback) {
                    callback.call();
                }
                $(this).unbind("mousemove", tmp).unbind("mouseup", arguments.callee);
                $("body").unbind("selectstart");
            });
            $("body").bind("selectstart", function () { return false; });
        });
    },
    Drag: function (target, handler, callback) {
        handler.mousedown(function (oEvent) {
            var oX = oEvent.pageX, oY = oEvent.pageY;
            function tmp(nEvent) {
                var dX = nEvent.pageX - oX,
                dY = nEvent.pageY - oY;
                oX = nEvent.pageX;
                oY = nEvent.pageY;
                target.css({ top: parseInt(target.css("top")) + dY, left: parseInt(target.css("left")) + dX });
            }
            $(document).mousemove(tmp).mouseup(function () {
                if (callback) {
                    callback.call();
                }
                $(this).unbind("mousemove", tmp).unbind("mouseup", arguments.callee);
                $("body").unbind("selectstart");
            });
            $("body").bind("selectstart", function () { return false; });
        });
    },
    insertAtCursor: (function (elem, str) {
        return document.selection ?
        function (elem, str) {
            elem.focus();
            document.selection.createRange().text = str;
        } :
        function (elem, str) {
            var oldValue = elem.value,
                curPos = elem.selectionStart;
            elem.value = oldValue.substring(0, elem.selectionStart) + str + oldValue.substring(elem.selectionEnd, oldValue.length);
            elem.selectionEnd = elem.selectionStart = (curPos += str.length);
        };
    })()
};
// ========================================
//         初   始   化
// ========================================

UI.resize();
$(window).resize(UI.resize);

// slide
$(".slider").mousedown(function (oEvent) {
    var lr = this.id.split("-")[1],
        c = (lr == "right") ? -1 : 1,
        oX = oEvent.pageX;
    $(document).mousemove(function (nEvent) {
        var d = c * (nEvent.pageX - oX); oX = nEvent.pageX;
        $("#middle").css("margin-" + lr, function (i, v) { return parseInt(v) + d; });
        $("#" + lr).css("width", function (i, v) { return parseInt(v) + d; });
    }).mouseup(function () {
        $(this).unbind("mousemove");
        $("body").unbind("selectstart");
    }); ;
    $("body").bind("selectstart", function () { return false; });
});

// 函数列表
$(".func-cate").click(function (e) {
    var target = e.target,
        isAdd = target.className.indexOf("icon-add") !== -1,
        isDelete = target.className.indexOf("icon-delete") !== -1,
        category = this.id.substr(1);
    if (isAdd) {        
        $.getScript("js/functions/" + category + ".js", function () {
            $(target).hide().prev().prev().show().parent().data("loaded", true);
        });
    } else if (isDelete) {
        var funcArr = DA[category], i, len;
        for (i = 0, len = funcArr.length; i < len; ++i) {
            window[funcArr[i]] = undefined;
        }
        $(target).hide().prev().hide().end().next().show().parent().data("loaded", false);
    } else {
        $(this).next().toggle();
    }
}).hover(function () {
    var jobj = $(this);
    if (jobj.data("loaded")) {
        jobj.children(".icon-delete").show();
    }
}, function () {
    $(this).children(".icon-delete").hide();
});

// show functions tips
$(".func-name").hover(function () {
    var jobj = $(this),
        offset = jobj.offset();
    jobj.toggleClass("func-name-hover");
    $("#tip-main").html(jobj.next().html()).parent().toggle().offset({ top: offset.top - 11, left: jobj.width() + 10 });
});

// 切换模式 code || cmd
$("#tab-cmd").click(function () {
    UI.curMode = $("#cmd");
});
$("#tab-code").click(function () {
    UI.curMode = $("#code");
});
$("#cmd").focus();  // 默认显示 cmd-mode
$(document).keydown(function () { UI.curMode.focus(); });

// cmd 执行
$("#cmd").keyup(function (e) {
    if (e.keyCode == 13) {
        var cmdstr = $.trim($(this).val());
        if (cmdstr == "") {
            return;
        }
        DA.run(cmdstr, true);
    } else if (e.keyCode == 38) {
        UI.rollBack(true);
    } else if (e.keyCode == 40) {
        UI.rollBack(false);
    }
});

// code 执行
$("#code").keyup(function (e) {
    if (e.ctrlKey && e.keyCode == 13) {    // enter 键
        var cmdstr = $.trim($(this).val());
        if (cmdstr != "") {
            DA.run(cmdstr, false);
        }
    }
}).keydown(function (e) {
    if (e.keyCode == 9) {   // tab 键
        UI.insertAtCursor(this, "    ");
        return false;
    }
});

// 双击重复命令
$("#history").dblclick(function (e) {
    var target = e.target,
        nodeName = target.nodeName.toUpperCase();
    if (nodeName == "SPAN" || nodeName == "I") {
        target = target.parentNode;
    }
    UI.curMode.val($(target).children()[1].innerHTML);
});

// tabs
$(".tabs").each(function () {
    new UI.Tabs($(this).children(), $(this).next().children());
});

// code-mode resize
new UI.Resize($("#code"), $("#codeResizer"));

// 预加载的函数
$(".pre-add").trigger("click");

// 设置code模式绘图示例代码
$("#code").val("// 示例，按Ctrl+Enter执行\nx=getArray(1,15,1)\ny=getArray(10,150,10)\nplot(x,y)");

// 隐藏  menus
$(document).click(function () {
    UI.clearMenu();
});

// ================= 绘图窗体 ====================

// drag
new UI.Drag($("#figure"), $.merge($("#f-header"), $("#f-drag")).mousedown(function () {
    $("#f-mask").show();    // 遮住 iframe
}), function () {
    $("#f-mask").hide();
});
// resize
new UI.Resize($.merge($("#figure"), $("#plotCanvas")), $("#f-resize").mousedown(function () {
    $("#f-mask").show();
}), function () {
    $("#f-mask").hide();
    plot.rePlot();
});
// get ready for plot,return 2d context
function readyForPlot() {
    $("#figure").show();
    $("#plotCanvas").show();
    $("#f-img").hide();
    $("#f-save").show();
    $("#f-back").hide();
    return $("#plotCanvas")[0].getContext("2d");
    //return new SVGContext(document.getElementById("plotCanvas"), "svgWrap");
}
// when plot done
readyForPlot.plotDone = function () {
    $.each(plot.lines, function (i, n) {
        $(n.parentNode).click(function () {
            if (this.uL) {
                this.uL.style.display = "block";
                this.dL.style.display = "block";
            } else {
                var d = n.getAttribute("d"),
                    reg = /\d+/g,
                    uD = d.replace(reg, function (word) {
                        return parseInt(word) + 2;
                    }),
                    dD = d.replace(reg, function (word) {
                        return parseInt(word) - 2;
                    }),
                    uDElem = SVG.plot(this, 'path', {
                        d: uD
                    }),
                    dDElem = SVG.plot(this, 'path', {
                        d: dD
                    });
                SVG.css(uDElem, {
                    "stroke": "#900b09",
                    "stroke-width": 2,
                    "fill": "none"
                });
                SVG.css(dDElem, {
                    "stroke": "#900b09",
                    "stroke-width": 2,
                    "fill": "none"
                });
                this.uL = uDElem;
                this.dL = dDElem;
            }
            var that = this;
            $(document).one("click", function (e) {
                that.uL.style.display = "none";
                that.dL.style.display = "none";
                plot.selectedLines = [];
            });
            if (plot["selectedLines"]) {
                plot.selectedLines.push(n);
            } else {
                plot.selectedLines = [n];
            }
            return false;
        });
    });
};

// commands
UI.figureCmds = {
    save: function () {
        if (!$("#plotCanvas")[0]["toDataURL"]) {
            alert("请使用屏幕截图保存图像！"); return;
        }
        $("#f-img").attr("src", $("#plotCanvas")[0].toDataURL()).show();
        $("#plotCanvas").hide();
        $("#f-save").hide();
        $("#f-back").show().click(function () {
            $("#plotCanvas").show();
            $("#f-img").hide();
            $("#f-save").show();
            $(this).hide();
            plot.rePlot();
        });
    },
    close: function () {
        $("#figure").hide();
    },
    showMenu: function (obj) {
        var id = obj[0].id.substr(2);
        UI.clearMenu();
        UI.dropMenu(obj, $("#" + id + "Menu"));
        return false;
    }
};

// 菜单栏
$("#f-menu").children().click(function () {
    return UI.figureCmds.showMenu($(this));
}).mouseover(function () {
    if (UI.curMenu.length > 0) {
        return UI.figureCmds.showMenu($(this));
    }
});

// 工具栏
$("#f-toolbar").click(function (e) {
    var id = e.target.id.substr(2);
    if (UI.figureCmds[id]) {
        UI.figureCmds[id]();
    }
});

// 菜单 UI
// drop menus & sub menus
$(".ctxMenu li").mouseover(function () {
    $(this).addClass("current").siblings().removeClass("current");
});
// sub menus
$(".submenu li").mouseout(function () {
    $(this).removeClass("current");
});
// 菜单功能
// drop menus
$("#colorMenu li").mouseover(function () {
    UI.subMenu($(this),$("#palette"));
});
$("#lineMenu li").mouseover(function () {
    UI.subMenu($(this),$("#num-series"));
});
// sub menus
$("#charMenu").click(function (e) {
    var char = e.target.innerHTML;
    plot.opt.char = char == "无" ? "" : char;
    if (plot["selectedLines"] && plot.selectedLines.length) {
        $.each(plot.selectedLines, function (i, n) {
            $(n).parent().children("text").each(function () {
                this.textContent = plot.opt.char;
            });
        });
    } else {
        plot.rePlot();
    }
});
$("#num-series").click(function (e) {
    var prevElem = $(this).data("prevElem"),
        width = parseInt(e.target.innerHTML),
        text = $.trim(prevElem.text().replace(/\d/g, ""));
    prevElem.children("span").html(width);
    switch (text) {
        case "曲线宽度":
            plot.opt.lineWidth = width;
            break;
        case "边框宽度":
            plot.opt.borderWidth = width;
            break;
        case "网格线宽度":
            plot.opt.gridWidth = width;
            break;
    }
    if (text == "曲线宽度" && plot["selectedLines"] && plot.selectedLines.length) {
        $.each(plot.selectedLines, function (i, n) {
            SVG.css(n, {
                "stroke-width": plot.opt.lineWidth
            });
        });
    } else {
        plot.rePlot();
    }
});
$("#palette").click(function (e) {
    var color = e.target.title,
        prevElem = $(this).data("prevElem"),
        text = $.trim(prevElem.text());
    prevElem.children("span").css("background-color", color).attr("title", color);
    switch (text) {
        case "点颜色":
            plot.opt.charColor = color;
            break;
        case "曲线颜色":
            plot.opt.lineColor = color;
            break;
        case "边框颜色":
            plot.opt.borderColor = color;
            break;
        case "网格颜色":
            plot.opt.gridColor = color;
            break;
        case "标签颜色":
            plot.opt.labelColor = color;
            break;
    }
    if (text == "曲线颜色" && plot["selectedLines"] && plot.selectedLines.length) {
        $.each(plot.selectedLines, function (i, n) {
            SVG.css(n, {
                "stroke": plot.opt.lineColor
            });
        });
    } else {
        plot.rePlot();
    }
});

// 生成调色板
(function () {
    var strHTML = "<table class='palette' cellspacing='0' cellpadding='0' border='0'>";
    var colors = "rgb(0, 0, 0):rgb(68, 68, 68):rgb(102, 102, 102):rgb(153, 153, 153):rgb(204, 204, 204):rgb(238, 238, 238):rgb(243, 243, 243):rgb(255, 255, 255):rgb(255, 0, 0):rgb(255, 153, 0):rgb(255, 255, 0):rgb(0, 255, 0):rgb(0, 255, 255):rgb(0, 0, 255):rgb(153, 0, 255):rgb(255, 0, 255):rgb(244, 204, 204):rgb(252, 229, 205):rgb(255, 242, 204):rgb(217, 234, 211):rgb(208, 224, 227):rgb(207, 226, 243):rgb(217, 210, 233):rgb(234, 209, 220):rgb(234, 153, 153):rgb(249, 203, 156):rgb(255, 229, 153):rgb(182, 215, 168):rgb(162, 196, 201):rgb(159, 197, 232):rgb(180, 167, 214):rgb(213, 166, 189):rgb(224, 102, 102):rgb(246, 178, 107):rgb(255, 217, 102):rgb(147, 196, 125):rgb(118, 165, 175):rgb(111, 168, 220):rgb(142, 124, 195):rgb(194, 123, 160):rgb(204, 0, 0):rgb(230, 145, 56):rgb(241, 194, 50):rgb(106, 168, 79):rgb(69, 129, 142):rgb(61, 133, 198):rgb(103, 78, 167):rgb(166, 77, 121):rgb(153, 0, 0):rgb(180, 95, 6):rgb(191, 144, 0):rgb(56, 118, 29):rgb(19, 79, 92):rgb(11, 83, 148):rgb(53, 28, 117):rgb(116, 27, 71):rgb(102, 0, 0):rgb(120, 63, 4):rgb(127, 96, 0):rgb(39, 78, 19):rgb(12, 52, 61):rgb(7, 55, 99):rgb(32, 18, 77):rgb(76, 17, 48)".split(":");
    for (var i = 0, len = colors.length; i < len; i += 8) {
        strHTML += "<tr class='tr" + (i / 8 + 1) + "'>";
        for (var j = 0; j < 8; ++j) {
            strHTML += "<td><a href='#' class='palette-cell' style='background-color:" + colors[i + j] + ";'title='" + colors[i + j] + "'></a></td>";
        }
        strHTML += "</tr>";
    }
    strHTML += "</table>";
    $("#palette").html(strHTML);
})();

