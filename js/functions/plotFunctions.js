﻿DA.plotFunctions = ["getArray", "plot"];

function getArray(start, end, step) {
    start = parseFloat(start);
    end = parseFloat(end);
    step = parseFloat(step);
    
    var a = [],
        flen = plot.flen,
        f = Math.max.call(Math, flen(start), flen(end),flen(step)),
        i = start;

    while (i <= end) {
        a.push(i);
        i += step;
        i = parseFloat(i.toFixed(f));
    }
    return a;
}

var plot = (function () {
    var pow = Math.pow,
        round = Math.round;

    // 将一个数化成 a,b 之间的数，通过除以或乘以10，并记下乘或除的次数
    function between(x, a, b) {
        var factor = 0;
        while (x < a) {
            x *= 10;
            factor -= 1;
        }
        while (x > b) {
            x /= 10;
            factor += 1;
        }
        return [x, factor];
    }
    // 确定 d ,即坐标间隔, 参数 di = max - min
    function getD(di) {
        var x_f = between(di, 0.5, 10), // 化到 0.5 - 10 之间
            di = x_f[0],
            factor = x_f[1],
            D;

        if (di >= 0.5 && di <= 1) {
            D = 0.1;
        } else if (di > 1 && di <= 2) {
            D = 0.2;
        } else if (di > 2 && di <= 5) {
            D = 0.5;
        } else if (di > 5 && di <= 10) {
            D = 1;
        } else {
            alert(di);   // debug
        }
        return D * pow(10, factor);
    }
    // 获取轴上标签
    function getPoints(min, max, d) {

        // 计算 minLabel 和 maxLabel，minLable是只取 min 的最高位，忽略低位后的值
        var minLabel, maxLabel,
            a_f = between(min, 0, 10);
        minLabel = a_f[0].toString().split(".")[0] * pow(10, a_f[1]);
        a_f = between(max, 0, 10);
        maxLabel = (a_f[0].toString().split(".")[0] + 1) * pow(10, a_f[1]);

        // 如果最小值是一位数，并且最大值至少是两位数，那最小值设为 0  ==》 此规则废弃，按下一规则
        // 如果最大值的位数比最小值大一位以上，最小值设为0
        var l = minLabel.toString().length;
        if (l < maxLabel.toString().length) {
            minLabel = 0;
        }
        // 保存小数位数
        var f = Math.max.call(Math, plot.flen(min), plot.flen(max), plot.flen(d));

        do {
            minLabel += d;
        } while (minLabel <= min)
        minLabel -= d;
        do {
            maxLabel -= d;
        } while (maxLabel >= max)
        maxLabel += d;

        return getArray(minLabel.toFixed(f), maxLabel.toFixed(f), d);
    }
    // 画线
    function line(ctx, x0, y0, x1, y1) {
        ctx.beginPath();
        ctx.moveTo(round(x0), round(y0));
        ctx.lineTo(round(x1), round(y1));
        ctx.stroke();
    }
    // options
    var opt = {
        char: "*",
        charColor: "rgb(11,83,148)",
        lineColor: "rgb(249,203,156)",
        borderColor: "rgb(153,0,255)",
        gridColor: "rgb(106,168,79)",
        labelColor: "#ff0000",
        lineWidth: 2,
        borderWidth: 2,
        gridWidth: 1
    };

    return function (xn, yn, user_opt) {
        // get context
        var ctx = readyForPlot();
        if (!ctx) {
            return false;
        }
        // save context for rePlot
        plot.ctx = ctx;

        // validate xn,yn and get min max
        var minX = Math.min.apply(Math, xn),
            minY = Math.min.apply(Math, yn),
            maxX = Math.max.apply(Math, xn),
            maxY = Math.max.apply(Math, yn);
        if (maxX == minX && maxY == minY) {
            return false;       //x,y不能同时为常量数组
        }

        // set options
        if (user_opt && user_opt.constructor == Object) {
            for (var item in user_opt) {
                if (!opt[item]) {
                    alert("没有此项设置：" + item);
                    return;
                }
                opt[item] = user_opt[item];
            }
        }
        plot.opt = opt;

        // get axis labels --> xP,yP
        var dX = getD(maxX - minX),
            dY = getD(maxY - minY),
            xP = getPoints(minX, maxX, dX),
            yP = getPoints(minY, maxY, dY);

        var w = ctx.canvas.width,
            h = ctx.canvas.height,
        // 图形的 边框宽度和高度
            bw = w - 110,
            bh = h - 105,
        // 网格的 宽度和高度
            iw = bw / (xP.length - 1),
            ih = bh / (yP.length - 1);
        // 清除
        ctx.clearRect(0, 0, w, h);
        // 移动原点，全部绘制完成后需重设原点
        ctx.translate(0.5, 0.5);
        // 绘制边框
        if (opt.borderWidth) {
            ctx.strokeStyle = opt.borderColor;
            ctx.lineWidth = opt.borderWidth;
            ctx.strokeRect(55, 65, bw, bh);
        }
        // 绘制网格
        if (opt.gridWidth) {
            ctx.strokeStyle = opt.gridColor;
            ctx.lineWidth = opt.gridWidth;

            // 竖线
            for (var i = 1; i <= xP.length - 2; ++i) {
                line(ctx, 55 + iw * i, 66, 55 + iw * i, bh + 65);
            }
            // 横线
            for (var i = 1; i <= yP.length - 2; ++i) {
                line(ctx, 56, ih * i + 65, bw + 55, ih * i + 65);
            }
        }
        // 绘制坐标轴标签
        ctx.fillStyle = opt.labelColor;
        ctx.font = "14px sans-serif";
        for (var i = 0; i < xP.length; ++i) {
            ctx.fillText(xP[i], 50 + i * iw, 80 + bh);
        }
        for (var i = 0; i < yP.length; ++i) {
            ctx.fillText(yP[i], 50 - (ctx.measureText(yP[i])).width, h - 38 - ih * i);
        }

        // 转换坐标
        var i = 0, x = [], y = [];
        for (i = 0; i < xn.length; ++i) {
            x[i] = (xn[i] - xP[0]) * iw / dX + 55;
        }
        for (i = 0; i < yn.length; ++i) {
            y[i] = bh - ((yn[i] - yP[0]) * ih / dY) + 65;
        }
        // 绘制线
        if (opt.lineWidth) {
            ctx.strokeStyle = opt.lineColor;
            ctx.lineWidth = opt.lineWidth;
            ctx.beginPath();
            ctx.moveTo(x[0], y[0]);
            for (i = 1; i < x.length; ++i) {
                ctx.lineTo(x[i], y[i]);
            }
            ctx.stroke();
        }
        // 绘制点
        var charW = ctx.measureText(opt.char).width / 2;
        ctx.fillStyle = opt.charColor;
        ctx.textBaseline = "middle";
        for (i = 0; i < x.length; ++i) {
            ctx.fillText(opt.char, x[i] - charW, y[i]);
        }
        ctx.textBaseline = "alphabetic";
        // 重置原点
        ctx.translate(-0.5, -0.5);

        // 在canvas上保存此次绘图命令参数，以便resize后重绘
        ctx.lastPlotArgs = arguments;
    };
})();

// 获取小数位数
plot.flen = function (n) {
    return n.toString().indexOf('.') == -1 ? 0 : n.toString().split('.')[1].length;
};
// 重绘
plot.rePlot = function () {
    this.apply(window, this.ctx.lastPlotArgs);
};