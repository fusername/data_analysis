﻿DA.sortFunctions = ["quickSort","quickSort2"];
function quickSort(a) {
    if (a.length == 0) return [];

    var left = [];
    var right = [];
    var pivot = a[0];
    for (var i = a.length; --i; ) {
        if (a[i] < pivot)
            left.push(a[i]);
        else
            right.push(a[i]);
    }

    return quickSort(left).concat(pivot, quickSort(right));
}

function quickSort2(a, left, right) {
    var pivot, i, j, temp;
    if (left < right) {
        i = left;
        j = right + 1;
        pivot = a[left];
        do {
            do {
                ++i;
            } while (a[i] < pivot);
            do {
                --j;
            } while (a[j] > pivot);
            if (i < j) {
                swap(a, i, j, temp);
            }
        } while (i < j);
        swap(a, left, j, temp);
        arguments.callee(a, left, j - 1);
        arguments.callee(a, j + 1, right);
    }
    return a;
}