﻿DA.mathFunctions = ["makeMat","matMulti","matRev","matVal","matRank"];
// 数组复制
Array.prototype.copy = function (start, end) {
    var s = start ? Math.max(0, start) : 0;
    var e = end ? Math.min(end, this.length) : this.length;
    var newArray = new Array();
    for (var i = s; i < e; ++i) {
        if (this[i].constructor == Array || this[i].constructor == String) {
            var temp = [].concat(this[i]);
            newArray.push(temp);
        } else {
            newArray.push(this[i]);
        }
    }
    return newArray;
}
// 生成矩阵
function makeMat(m, n, k) {
    var mat = new Array(m);
    var v = k ? k : 0;
    for (var i = 0; i < m; ++i) {
        var matTemp = new Array(n);
        for (var j = 0; j < n; ++j) {
            matTemp[j] = v;
        }
        mat[i] = matTemp;
    }
    return mat;
}
// 矩阵相乘
function matMulti(mat1, mat2) {
    var m = mat1.length;
    var n, k;
    if (!(n = mat1[0].length)) {
        for (var i = 0; i < m; ++i) {
            var temp = new Array();
            temp.push(mat1[i]);
            mat1[i] = temp;
        }
        n = 1;
    }
    if (!(k = mat2[0].length)) {
        for (var i = 0; i < m; ++i) {
            var temp = new Array();
            temp.push(mat2[i]);
            mat2[i] = temp;
        }
        k = 1;
    }
    var i, j, t;
    var mat = makeMat(m, k);
    for (i = 0; i < m; ++i) {
        for (j = 0; j < k; ++j) {
            for (t = 0; t < n; ++t) {
                mat[i][j] += mat1[i][t] * mat2[t][j];
            }
        }
    }
    return mat;
}
// 一般矩阵求逆
function matRev(mat) {
    var a = mat.copy();   // 复制
    var n = a.length;
    var is = new Array(n);
    var js = new Array(n);
    var i, j, k, d, p;
    for (k = 0; k <= n - 1; ++k) {
        d = 0.0;
        for (i = k; i <= n - 1; ++i) {
            for (j = k; j <= n - 1; ++j) {
                p = Math.abs(a[i][j]);
                if (p > d) { d = p; is[k] = i; js[k] = j; }
            }
        }
        if (d + 1.0 == 1.0) {
            delete is; delete js;
            alert("A为奇异矩阵！没有逆矩阵。");
            return false;
        }
        if (is[k] != k) {
            for (j = 0; j <= n - 1; ++j) {
                p = a[k][j]; a[k][j] = a[is[k]][j]; a[is[k]][j] = p;
            }
        }
        if (js[k] != k) {
            for (i = 0; i <= n - 1; ++i) {
                p = a[i][k]; a[i][k] = a[i][js[k]]; a[i][js[k]] = p;
            }
        }
        a[k][k] = 1.0 / a[k][k];
        for (j = 0; j <= n - 1; ++j) {
            if (j != k) a[k][j] = a[k][j] * a[k][k];
        }
        for (i = 0; i <= n - 1; ++i) {
            if (i != k) {
                for (j = 0; j <= n - 1; ++j) {
                    if (j != k) a[i][j] = a[i][j] - a[i][k] * a[k][j];
                }
            }
        }
        for (i = 0; i <= n - 1; ++i) {
            if (i != k) a[i][k] = -a[i][k] * a[k][k];
        }
    }
    for (k = n - 1; k >= 0; --k) {
        if (js[k] != k) {
            for (j = 0; j <= n - 1; ++j) {
                p = a[k][j]; a[k][j] = a[js[k]][j]; a[js[k]][j] = p;
            }
        }
        if (is[k] != k) {
            for (i = 0; i <= n - 1; ++i) {
                p = a[i][k]; a[i][k] = a[i][is[k]]; a[i][is[k]] = p;
            }
        }
    }
    delete is; delete js;
    return a;
}
// 求一般行列式的值
function matVal(mat) {
    var a = mat.copy();   // 复制
    var n = a.length;
    var i, j, k, is, js, f = 1.0, q, d, det = 1.0;
    for (k = 0; k <= n - 2; ++k) {
        q = 0.0;
        for (i = k; i <= n - 1; ++i) {
            for (j = k; j <= n - 1; ++j) {
                d = Math.abs(a[i][j]);
                if (d > q) {
                    q = d; is = i; js = j;
                }
            }
        }
        if (q + 1.0 == 1.0) det = 0.0;
        else {
            if (is != k) {
                f = -f;
                for (j = k; j <= n - 1; ++j) {
                    d = a[k][j]; a[k][j] = a[is][j]; a[is][j] = d;
                }
            }
            if (js != k) {
                f = -f;
                for (i = k; i <= n - 1; ++i) {
                    d = a[i][js]; a[i][js] = a[i][k]; a[i][k] = d;
                }
            }
            det *= a[k][k];
            for (i = k + 1; i <= n - 1; ++i) {
                d = a[i][k] / a[k][k];
                for (j = k + 1; j <= n - 1; ++j) {
                    a[i][j] -= d * a[k][j];
                }
            }
        }
    }
    det *= f * a[n - 1][n - 1];
    return det;
}
// 求矩阵的秩
function matRank(mat) {
    var mat2 = mat.copy();    // 复制
    var m = mat2.length;
    var n;
    if (!(n = mat2[0].length)) {
        for (var i = 0; i < m; ++i) {
            var temp = new Array();
            temp.push(mat2[i]);
            mat2[i] = temp;
        }
        n = 1;
    }
    var i, j, nn, is, js, l, q, d, k;
    nn = m;
    if (m > n) nn = n;
    k = 0;
    for (l = 0; l <= nn - 1; ++l) {
        q = 0.0;
        for (i = 1; i <= m - 1; ++i) {
            for (j = 1; j <= n - 1; ++j) {
                d = Math.abs(mat2[i][j]);
                if (d > q) { q = d; is = i; js = j; }
            }
        }
        if (q + 1.0 == 1.0) break;
        ++k;
        if (is != l) {
            for (j = 1; j <= n - 1; ++j) {
                d = mat2[l][j]; mat2[l][j] = mat2[is][j]; mat2[is][j] = d;
            }
        }
        if (js != l) {
            for (i = l; i <= m - 1; ++i) {
                d = mat2[i][js]; mat2[i][js] = mat2[i][l]; mat2[i][l] = d;
            }
        }
        for (i = l + 1; i <= n - 1; ++i) {
            d = mat2[i][l] / mat2[l][l];
            for (j = l + 1; j <= n - 1; ++j) {
                mat2[i][j] -= d * mat2[l][j];
            }
        }
    }
    return k;
}